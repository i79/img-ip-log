<?php
if (isset($_GET["f"])) {
	/* We put ip logging as top priority, then we fetch file */
	$vFilename = $_GET["f"];
	/* Create variable, assign to file handle */
	$vFileHnd = fopen("iplist.log", "a");
	/* Write IP to FileHnd, we use bit of php haxx */
	fwrite($vFileHnd, "$_SERVER[REMOTE_ADDR]\r\n");
	/* Close FileHnd */
	fclose($vFileHnd);
	/* Reading image comes last, not vulnerable */
	/* .htaccess RegEx allows only png|jpg|gif|bmp extension */
	if (file_exists($vFilename)) {
		$vExt = explode(".", $vFilename);
		header("content-type: image/".$vExt[count($vExt)-1]);
		readfile($vFilename);
		/* Terminate file */
		exit;
	}
}
?>File not found.
