# Image IP logger

Greetz to Cysecor, here man, a real no-redirect logger

This __does not__ require editing, __unless__ you want to rename "uploads" virtual folder, otherwise no edits needed.

Once you upload this thing to your hosting, all you need to do is upload it to any folder, whether public_html, or some long path.\
You will need to know the path to it, this is how the bait URL syntax looks like:

http://localhost/{yourPath}/uploads/{anyFileName}.{acceptableExtension}

**{yourPath}** is a folder you where you put the files\
**{anyFileName}** is existing file name (without extension)\
**{acceptableExtension}** is extension name of the existing file

Basically, the bait URL varies on the existing images on your site.

Example, if **public_html**:

http://localhost/uploads/cool.gif

Example, if path is "**/very/long/path/**"

http://localhost/very/long/path/uploads/cool.gif

The logger is not vulnerable to anything, except spam.
